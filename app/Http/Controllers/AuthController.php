<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio()
    {
        return view('page.register');
    }

    public function kirim(Request $request)
    {
        // dd($request->all());
        $nfirst   = $request['first'];
        $nlast = $request['last'];

        return view('page.welcome', compact("nfirst", "nlast"));
    }

}
